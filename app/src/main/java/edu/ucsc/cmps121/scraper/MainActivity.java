package edu.ucsc.cmps121.scraper;

import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity implements ListAdapter{
    List<Bitmap> bitmaps = new ArrayList<Bitmap>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView imageListView = (ListView)findViewById(R.id.image_list_view);
        imageListView.setAdapter(this);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                URL url = null;
                String document = null;
                //Download document
                try {
                    url = new URL("https://www.nytimes.com");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    InputStream inputStream = connection.getInputStream();
                    Scanner scanner = new Scanner(inputStream);
                    StringBuilder stringBuilder = new StringBuilder();
                    while(scanner.hasNext()){
                        //Log.i("inside", "inside");
                        stringBuilder.append(scanner.nextLine());
                    }
                    document = stringBuilder.toString();
                    Log.i("Tag", document);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //Process document for images
                List<URL> imageURLs = new ArrayList<URL>();
                while(true) {
                    int imageTagStart = document.indexOf("<img src=\"");
                    if(imageTagStart < 0) break;

                        int imageURLStart = imageTagStart + "<img src=\"".length();
                        int imageTagEnd = document.indexOf("\"", imageURLStart);
                        if (imageTagEnd < 0)
                            break;
                        URL imageURL = null;
                        try {
                                String imageTagString = document.substring(imageURLStart, imageTagEnd);
                                imageURL = new URL(imageTagString);
                            } catch (MalformedURLException e) {
                            }
                        if (imageURL != null) {
                            Log.i("Image Tag", imageURL.toString());
                            imageURLs.add(imageURL);
                        }
                        document = document.substring(imageTagEnd);
                }
                for(URL imageURL: imageURLs){
                    Bitmap bitmap = null;
                    try {
                        bitmap = BitmapFactory.decodeStream(imageURL.openStream());
                    } catch (IOException e) {
                    }
                    if(bitmap != null){
                        Log.i("Bitmap", "Found bitmap of size" + bitmap.getHeight() + "x" + bitmap.getHeight());
                        bitmaps.add(bitmap);
                    }
                }

                //Post
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ListView imageListView = (ListView) findViewById(R.id.image_list_view);
                        imageListView.invalidateViews();
                    }
                });
            }
        });
        thread.start();


        Log.i("start", "start");

    }


    @Override
    public int getCount() {
        return bitmaps.size();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Object getItem(int i) {
        return bitmaps.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Bitmap image = (Bitmap)getItem(i);
        ImageView imageView = new ImageView(this);
        imageView.setImageBitmap(image);
        return imageView;
    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }



    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int i) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }
    @Override
    public boolean hasStableIds() {
        return false;
    }




}
